use std::{collections::BTreeMap, fmt::Debug};

use typed_generational_arena::{Arena, IgnoreGeneration, Index};
use utils::{first_char, longest_common_prefix};
mod utils;

#[derive(Debug, Clone)]
struct RTNode<T> {
    data: Option<T>,
    children: BTreeMap<char, RTEdge>,
}

impl<T> RTNode<T> {
    fn new(data: Option<T>) -> Self {
        Self {
            data,
            children: Default::default(),
        }
    }

    fn empty() -> Self {
        RTNode::new(None)
    }

    fn of(data: T) -> Self {
        RTNode::new(Some(data))
    }

    fn has_data(&self) -> bool {
        self.data.is_some()
    }
}

#[derive(Debug, Clone)]
struct RTEdge {
    destination: usize,
    label: String,
}

impl RTEdge {
    fn new(destination: usize, label: String) -> Self {
        Self { destination, label }
    }
}

#[derive(Debug, Clone)]
pub struct RadixTrie<T> {
    node_arena: Arena<RTNode<T>, usize, IgnoreGeneration>,
    root_idx: usize,
}

impl<T> RadixTrie<T> {
    fn _insert_node(&mut self, node: RTNode<T>) -> usize {
        self.node_arena.insert(node).to_idx()
    }

    fn _get_node(&self, node_idx: usize) -> &RTNode<T> {
        self.node_arena.get(Index::from_idx(node_idx)).unwrap()
    }

    fn _get_node_mut(&mut self, node_idx: usize) -> &mut RTNode<T> {
        self.node_arena.get_mut(Index::from_idx(node_idx)).unwrap()
    }

    fn _remove_node(&mut self, node_idx: usize) {
        self.node_arena.remove(Index::from_idx(node_idx));
    }

    fn _match_edge<'a, 'b>(
        &'a self,
        node_idx: usize,
        s: &'b str,
    ) -> Option<(&'a RTEdge, &'b str, &'b str, &'a str)> {
        let c = first_char(s);
        let node = self._get_node(node_idx);

        match node.children.get(&c) {
            Some(edge) => {
                let (prefix, suffix_s, suffix_edge) = longest_common_prefix(s, &edge.label);
                Some((edge, prefix, suffix_s, suffix_edge))
            }
            None => None,
        }
    }

    fn _search<'a, 'b>(&'a self, node_idx: usize, s: &'b str) -> Option<&'a T> {
        if s.is_empty() {
            let node = self._get_node(node_idx);
            node.data.as_ref()
        } else {
            match self._match_edge(node_idx, s) {
                Some((edge, _, suffix_s, suffix_edge)) => {
                    if suffix_edge.is_empty() {
                        self._search(edge.destination, suffix_s)
                    } else {
                        None
                    }
                }
                None => None,
            }
        }
    }

    fn _insert<'a, 'b>(&'a mut self, node_idx: usize, s: &'b str, data: T) {
        if s.is_empty() {
            let node = self._get_node_mut(node_idx);
            node.data = Some(data);
        } else {
            let c = first_char(s);
            match self._match_edge(node_idx, &s) {
                Some((edge, common_prefix, suffix_s, suffix_edge)) => {
                    if suffix_edge.is_empty() {
                        let destination = edge.destination;
                        self._insert(destination, suffix_s, data);
                    } else {
                        let destination = edge.destination;
                        let common_prefix_owned = common_prefix.to_owned();
                        let suffix_edge_owned = suffix_edge.to_owned();

                        let mut bridge = RTNode::<T>::empty();
                        let c_edge = first_char(suffix_edge);
                        bridge
                            .children
                            .insert(c_edge, RTEdge::new(destination, suffix_edge_owned));
                        let bridge_idx = self._insert_node(bridge);

                        let node = self._get_node_mut(node_idx);
                        node.children
                            .insert(c, RTEdge::new(bridge_idx, common_prefix_owned));

                        self._insert(bridge_idx, suffix_s, data)
                    }
                }
                None => {
                    let new_node_idx = self._insert_node(RTNode::of(data));
                    let node = self._get_node_mut(node_idx);
                    node.children
                        .insert(c, RTEdge::new(new_node_idx, s.to_owned()));
                }
            }
        }
    }

    fn _is_passthrough(&self, node_idx: usize) -> bool {
        let node = self._get_node(node_idx);
        node.data.is_none() && node.children.len() == 1
    }

    fn _remove<'a, 'b>(&'a mut self, node_idx: usize, s: &'b str) -> (bool, bool) {
        if s.is_empty() {
            let node = self._get_node_mut(node_idx);
            node.data = None;
            let should_prune = node.children.len() == 0;
            if should_prune {
                self._remove_node(node_idx);
            }
            (true, should_prune)
        } else {
            match self._match_edge(node_idx, &s) {
                Some((edge, _, suffix_s, suffix_edge)) if suffix_edge.is_empty() => {
                    let dest_idx = edge.destination;
                    let (deleted, should_prune) = self._remove(dest_idx, suffix_s);
                    if deleted {
                        let c = first_char(s);
                        if should_prune {
                            let node = self._get_node_mut(node_idx);
                            node.children.remove(&c);
                        } else if self._is_passthrough(dest_idx) {
                            let next_edge =
                                self._get_node(dest_idx).children.values().nth(0).unwrap();
                            let curr_edge = &self._get_node(node_idx).children[&c];
                            let merged_label = curr_edge.label.to_owned() + &next_edge.label;
                            let destination = next_edge.destination;
                            let node = self._get_node_mut(node_idx);

                            node.children
                                .insert(c, RTEdge::new(destination, merged_label));
                            self._remove_node(dest_idx);
                        }
                    }
                    (deleted, false)
                }
                _ => (false, false),
            }
        }
    }

    fn _longest_prefix(&self, node_idx: usize, s: &str, prefix: &mut String) -> bool {
        if s.is_empty() {
            self._get_node(node_idx).has_data()
        } else {
            match self._match_edge(node_idx, s) {
                Some((edge, common_prefix, suffix_s, suffix_edge)) if suffix_edge.is_empty() => {
                    prefix.push_str(common_prefix);
                    let result = self._longest_prefix(edge.destination, suffix_s, prefix);
                    if result {
                        return result;
                    }
                    prefix.truncate(prefix.len() - common_prefix.len());
                }
                _ => {}
            };
            self._get_node(node_idx).has_data()
        }
    }

    fn _node_with_prefix(
        &self,
        node_idx: usize,
        prefix: &str,
        buffer: &mut String,
    ) -> Option<usize> {
        if prefix.is_empty() {
            Some(node_idx)
        } else {
            match self._match_edge(node_idx, prefix) {
                Some((edge, common_prefix, suffix_s, suffix_edge)) => {
                    if suffix_edge.is_empty() {
                        buffer.push_str(common_prefix);
                        self._node_with_prefix(edge.destination, suffix_s, buffer)
                    } else if suffix_s.is_empty() {
                        buffer.push_str(&edge.label);
                        Some(edge.destination)
                    } else {
                        None
                    }
                }
                None => None,
            }
        }
    }
}

struct RadixTrieIterator<'a, T> {
    trie: &'a RadixTrie<T>,
    stack: Vec<(usize, String)>,
}

impl<'a, T> RadixTrieIterator<'a, T> {
    fn new(trie: &'a RadixTrie<T>, initial_node_data: (usize, String)) -> Self {
        let mut stack = Vec::new();
        stack.push(initial_node_data);
        Self { trie, stack }
    }

    fn empty(trie: &'a RadixTrie<T>) -> Self {
        Self {
            trie,
            stack: Vec::new(),
        }
    }
}

impl<'a, T> Iterator for RadixTrieIterator<'a, T> {
    type Item = (String, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some((node_idx, prefix)) = self.stack.pop() {
                let node = self.trie._get_node(node_idx);
                for edge in node.children.values().rev() {
                    let mut new_prefix = prefix.clone();
                    new_prefix.push_str(&edge.label);
                    self.stack.push((edge.destination, new_prefix));
                }
                if node.has_data() {
                    return Some((prefix, node.data.as_ref().unwrap()));
                }
            } else {
                return None;
            }
        }
    }
}

impl<T> RadixTrie<T>
where
    T: Debug,
{
    fn _dump(&self, node_idx: usize, level: usize) {
        let node = self._get_node(node_idx);
        for _ in 0..level {
            print!("\t");
        }
        println!("[{:?}]", node.data);
        for edge in node.children.values() {
            for _ in 0..level + 1 {
                print!("\t");
            }
            println!("-{}->", edge.label);
            self._dump(edge.destination, level + 2);
        }
    }
}

impl<T> Default for RadixTrie<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> RadixTrie<T> {
    pub fn new() -> Self {
        let mut node_arena = Arena::new();
        let root_idx = node_arena.insert(RTNode::empty()).arr_idx();

        Self {
            node_arena,
            root_idx,
        }
    }

    pub fn search<'a, 'b>(&'a self, s: &'b str) -> Option<&'a T> {
        self._search(self.root_idx, s)
    }

    pub fn insert<'a, 'b>(&'a mut self, s: &'b str, data: T) {
        self._insert(self.root_idx, s, data);
    }

    pub fn remove<'a, 'b>(&'a mut self, s: &'b str) {
        self._remove(self.root_idx, s);
    }

    pub fn longest_prefix<'a, 'b>(&'a self, s: &'b str) -> Option<String> {
        let mut prefix = String::new();
        if self._longest_prefix(self.root_idx, s, &mut prefix) {
            Some(prefix)
        } else {
            None
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = (String, &T)> {
        RadixTrieIterator::new(self, (self.root_idx, String::new()))
    }

    pub fn iter_with_prefix(&self, prefix: &str) -> impl Iterator<Item = (String, &T)> {
        let mut buffer = String::new();
        if let Some(node_idx) = self._node_with_prefix(self.root_idx, prefix, &mut buffer) {
            RadixTrieIterator::new(self, (node_idx, buffer))
        } else {
            RadixTrieIterator::empty(self)
        }
    }
}

impl<T> RadixTrie<T>
where
    T: Debug,
{
    pub(crate) fn dump(&self) {
        self._dump(self.root_idx, 0);
    }
}
#[cfg(test)]
mod tests {
    use crate::RadixTrie;

    #[test]
    fn test_sample() {
        let mut tree = RadixTrie::<i32>::new();
        tree.insert("hello", 1);
        tree.insert("world", 2);
        tree.insert("worldwide", 3);
        tree.insert("worm", 4);
        tree.insert("wor", 5);

        tree.insert("hey", 6);
        tree.insert("heap", 7);
        tree.insert("heal", 7);
        tree.insert("helm", 7);
        tree.insert("help", 8);

        tree.dump();
        //        println!("{:?}", tree);
        for (key, &data) in tree.iter_with_prefix("h") {
            println!("{} -> {}", key, data);
        }

        assert_eq!(tree.search("hello"), Some(1).as_ref());
        assert_eq!(tree.search("hel"), None);
        assert_eq!(tree.search("world"), Some(2).as_ref());
        assert_eq!(tree.search("worldwide"), Some(3).as_ref());
        assert_eq!(tree.search("worldwides"), None);

        assert_eq!(tree.search("worm"), Some(4).as_ref());
        assert_eq!(tree.search("wor"), Some(5).as_ref());

        tree.remove("worm");

        assert_eq!(tree.search("worm"), None);
        assert_eq!(tree.search("wor"), Some(5).as_ref());

        tree.dump();
        tree.remove("wor");
        tree.dump();
        assert_eq!(
            tree.longest_prefix("worldwideweb"),
            Some("worldwide".to_owned())
        );
        assert_eq!(tree.longest_prefix("worldtour"), Some("world".to_owned()));
        assert_eq!(tree.longest_prefix("hella"), None);
        assert_eq!(tree.longest_prefix("hello"), Some("hello".to_owned()));
        assert_eq!(tree.longest_prefix("helloworld"), Some("hello".to_owned()));

        let tree2 = tree.clone();
        tree2.dump();
    }
}
