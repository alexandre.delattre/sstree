pub fn longest_common_prefix<'a, 'b>(s1: &'a str, s2: &'b str) -> (&'a str, &'a str, &'b str) {
    let mut chars1 = s1.char_indices();
    let mut chars2 = s2.char_indices();

    let n = loop {
        match (chars1.next(), chars2.next()) {
            (Some((i, c1)), Some((_, c2))) => {
                if c1 != c2 {
                    break i;
                }
            }
            (None, Some(_)) => break s1.len(),
            (Some(_), None) => break s2.len(),
            (None, None) => break s1.len(),
        }
    };

    let common_prefix = &s1[..n];
    let suffix1 = &s1[n..];
    let suffix2 = &s2[n..];

    (common_prefix, suffix1, suffix2)
}

pub fn first_char(s: &str) -> char {
    s.chars().next().unwrap()
}

#[cfg(test)]
mod tests {
    use super::longest_common_prefix;

    #[test]
    fn test_longest_common_prefix() {
        assert_eq!(
            longest_common_prefix("foobiz", "foobar"),
            ("foob", "iz", "ar")
        );
        assert_eq!(longest_common_prefix("none", "mone"), ("", "none", "mone"));
        assert_eq!(longest_common_prefix("", "mone"), ("", "", "mone"));
        assert_eq!(
            longest_common_prefix("elephant", "elevation"),
            ("ele", "phant", "vation")
        );
        assert_eq!(
            longest_common_prefix("éléphant", "élévation"),
            ("élé", "phant", "vation")
        );
        assert_eq!(longest_common_prefix("", ""), ("", "", ""));
        assert_eq!(longest_common_prefix("abc", ""), ("", "abc", ""));
        assert_eq!(longest_common_prefix("", "abc"), ("", "", "abc"));
        assert_eq!(longest_common_prefix("ab", "abc"), ("ab", "", "c"));
        assert_eq!(longest_common_prefix("abc", "ab"), ("ab", "c", ""));
        assert_eq!(longest_common_prefix("abc", "abc"), ("abc", "", ""));
    }
}
