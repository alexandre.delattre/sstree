# sstree-pure-js

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test sstree-pure-js` to execute the unit tests via [Jest](https://jestjs.io).

## Running lint

Run `nx lint sstree-pure-js` to execute the lint via [ESLint](https://eslint.org/).
