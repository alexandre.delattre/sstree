import { Point, SsTree } from '@sstree-example/sstree-pure-js';
import { performance, PerformanceObserver } from 'perf_hooks';
import { Point2D, Point3D, SSTree2D, SSTree3D } from 'sstree';

function testWasm() {
  const tree = new SSTree2D();
  console.log('Inserting');
  for (let x = 0; x < 45; x++) {
    for (let y = 0; y < 45; y++) {
      tree.insert(`(${x}, ${y})`, new Point2D(x, y));
    }
  }

  const point = new Point2D(22.1, 22.8);
  console.log(tree.nearestNeighbor(point));
  console.log(tree.sphericalSearch(point, 3));
  console.log(tree.mNearestNeighbors(new Point2D(22.1, 22.8), 5));

  tree.free();
}

function testWasm3d() {
  const tree = new SSTree3D();
  console.log('Inserting');
  for (let x = 0; x < 45; x++) {
    for (let y = 0; y < 45; y++) {
      for (let z = 0; z < 45; z++) {
        tree.insert(`(${x}, ${y}, ${z})`, new Point3D(x, y, z));
      }
    }
  }

  const point = new Point3D(22.1, 22.8, 42.3)
  console.log(tree.nearestNeighbor(point));
  console.log(tree.sphericalSearch(point, 3));

  tree.free();
}

function testPureJs() {
  const tree = new SsTree([], 32);

  console.log('Inserting');
  for (let x = 0; x < 45; x++) {
    for (let y = 0; y < 45; y++) {
      const p = new Point(x, y);
      p.data = `(${x}, ${y})`;
      tree.add(p);
    }
  }
  console.log(tree);

  console.log(tree.nearestNeighbour(new Point(22.1, 22.8)).data);
  console.log(
    Array.from(tree.pointsWithinDistanceFrom(new Point(22.1, 22.8), 3)).map(
      (p: Point) => p.data
    )
  );
}

function main() {
  const obs = new PerformanceObserver((list) => {
    console.log(list.getEntries());
    //  obs.disconnect();
  });
  obs.observe({ entryTypes: ['function'] });

  performance.timerify(testWasm)();
  performance.timerify(testWasm3d)();
  performance.timerify(testPureJs)();

  console.log('done');
}

main();
