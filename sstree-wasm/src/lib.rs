mod utils;

use sstree::{
    geometry::{Box, Point, Sphere},
    ss_tree::SSTree,
};
use wasm_bindgen::prelude::*;

use crate::utils::set_panic_hook;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::Wee < Alloc::INIT;

#[wasm_bindgen]
pub struct SSTree2D {
    tree: SSTree<JsValue, 2>,
}

#[wasm_bindgen]
pub struct Point2D {
    pub x: f64,
    pub y: f64,
}

#[wasm_bindgen]
impl Point2D {
    #[wasm_bindgen(constructor)]
    pub fn new(x: f64, y: f64) -> Self {
        Point2D { x, y }
    }
}

impl From<&Point2D> for [f64; 2] {
    fn from(p: &Point2D) -> Self {
        [p.x, p.y]
    }
}

#[wasm_bindgen]
impl SSTree2D {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        init_lib();
        let tree = SSTree::new();
        SSTree2D { tree }
    }

    pub fn insert(&mut self, item: JsValue, point: &Point2D) {
        self.tree.insert(Point::new(point.into(), item));
    }

    #[wasm_bindgen(js_name = "nearestNeighbor")]
    pub fn neareast_neighbor(&self, point: &Point2D) -> JsValue {
        self.tree
            .neareast_neighbor(&point.into())
            .map(|p| p.data.clone())
            .unwrap_or(JsValue::NULL)
    }

    #[wasm_bindgen(js_name = "mNearestNeighbors")]
    pub fn m_neareast_neighbors(&self, point: &Point2D, m: usize) -> Vec<JsValue> {
        self.tree
            .m_neareast_neighbors(&point.into(), m)
            .iter()
            .map(|p| p.data.clone())
            .collect()
    }

    #[wasm_bindgen(js_name = "sphericalSearch")]
    pub fn spherical_search(&self, point: &Point2D, r: f64) -> Vec<JsValue> {
        self.tree
            .region_search(&Sphere::new(point.into(), r))
            .iter()
            .map(|p| p.data.clone())
            .collect()
    }

    #[wasm_bindgen(js_name = "boxSearch")]
    pub fn box_search(&self, min: &Point2D, max: &Point2D) -> Vec<JsValue> {
        self.tree
            .region_search(&Box::new(min.into(), max.into()))
            .iter()
            .map(|p| p.data.clone())
            .collect()
    }
}

#[wasm_bindgen]
pub struct Point3D {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

#[wasm_bindgen]
impl Point3D {
    #[wasm_bindgen(constructor)]
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Point3D { x, y, z }
    }
}

impl From<&Point3D> for [f64; 3] {
    fn from(p: &Point3D) -> Self {
        [p.x, p.y, p.z]
    }
}

#[wasm_bindgen]
pub struct SSTree3D {
    tree: SSTree<JsValue, 3>,
}

#[wasm_bindgen]
impl SSTree3D {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        init_lib();
        let tree = SSTree::with_capacity(64);
        SSTree3D { tree }
    }

    pub fn insert(&mut self, item: JsValue, point: &Point3D) {
        self.tree.insert(Point::new(point.into(), item));
    }

    #[wasm_bindgen(js_name = "nearestNeighbor")]
    pub fn neareast_neighbor(&self, point: &Point3D) -> JsValue {
        self.tree
            .neareast_neighbor(&point.into())
            .map(|p| p.data.clone())
            .unwrap_or(JsValue::NULL)
    }

    #[wasm_bindgen(js_name = "mNearestNeighbors")]
    pub fn m_neareast_neighbors(&self, point: &Point3D, m: usize) -> Vec<JsValue> {
        self.tree
            .m_neareast_neighbors(&point.into(), m)
            .iter()
            .map(|p| p.data.clone())
            .collect()
    }

    #[wasm_bindgen(js_name = "sphericalSearch")]
    pub fn spherical_search(&self, point: &Point3D, r: f64) -> Vec<JsValue> {
        self.tree
            .region_search(&Sphere::new(point.into(), r))
            .iter()
            .map(|p| p.data.clone())
            .collect()
    }

    #[wasm_bindgen(js_name = "boxSearch")]
    pub fn box_search(&self, min: &Point3D, max: &Point3D) -> Vec<JsValue> {
        self.tree
            .region_search(&Box::new(min.into(), max.into()))
            .iter()
            .map(|p| p.data.clone())
            .collect()
    }
}

fn init_lib() {
    set_panic_hook();
}
