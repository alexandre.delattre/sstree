use smallvec::SmallVec;

use crate::geometry::{Sphere, Point};

use super::float_trait::Float;

trait HasBoundingSphere<F, const K: usize> {
    fn get_bounding_sphere(&self) -> &Sphere<F, K>;
}

#[derive(Debug)]
pub struct SSNode<F, I, const K: usize, const M: usize> {
    pub bounding_sphere: Sphere<F, K>,
    pub children: SmallVec<[I; M]>,
}

impl<F, I, const K: usize, const M: usize> SSNode<F, I, K, M>
where
    F: Float,
{
    pub fn new_empty() -> Self {
        let bounding_sphere = Sphere::new_empty();
        let children = SmallVec::new();
        Self {
            bounding_sphere,
            children,
        }
    }

    pub fn with_children_pair(new_child1: I, new_child2: I) -> Self {
        let bounding_sphere = Sphere::new_empty();
        let mut children = SmallVec::new();
        children.push(new_child1);
        children.push(new_child2);
        Self {
            bounding_sphere,
            children,
        }
    }
}

impl<F, I, const K: usize, const M: usize> HasBoundingSphere<F, K> for SSNode<F, I, K, M> {
    fn get_bounding_sphere(&self) -> &Sphere<F, K> {
        &self.bounding_sphere
    }
}

#[derive(Debug)]
pub struct SSLeaf<T, F, const K: usize, const M: usize> {
    pub bounding_sphere: Sphere<F, K>,
    pub points: SmallVec<[Point<T, F, K>; M]>,
}

impl<T, F, const K: usize, const M: usize> SSLeaf<T, F, K, M>
where
    F: Float,
{
    pub fn new_empty() -> Self {
        let bounding_sphere = Sphere::new_empty();
        let points = SmallVec::new();
        Self {
            bounding_sphere,
            points,
        }
    }
}

impl<T, F, const K: usize, const M: usize> HasBoundingSphere<F, K> for SSLeaf<T, F, K, M> {
    fn get_bounding_sphere(&self) -> &Sphere<F, K> {
        &self.bounding_sphere
    }
}
