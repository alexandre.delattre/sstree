//!
//! Implements a Similarity Search Tree [SSTree](ss_tree::SSTree) 
//! for efficient K-dimensional nearest-neighbor and region based search
//! 
mod float_trait;
pub mod node_allocator;
pub mod geometry;
mod ss_node;
mod min_m_elem;
pub mod ss_tree;

#[cfg(test)]
mod tests {}
