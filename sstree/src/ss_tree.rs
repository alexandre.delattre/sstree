use std::usize;

use smallvec::SmallVec;

use crate::{
    float_trait::Float,
    geometry::{Point, Region, Sphere},
    min_m_elem::MinMElements,
    node_allocator::{BiArenaNodeAllocator, NodeAllocator, NodeRef, NodeRefMut},
    ss_node::{SSLeaf, SSNode},
};

///
/// SSTree implements a K-dimensional similarity search tree.
///
/// For maximum flexibility and performance a lot of choices are left at compile time using generics
/// - T the type of data associated to points stored in the tree
/// - F the type of float ([f32] or [f64])
/// - K the dimension
/// - MAX the maximum number stored per leaf
/// - MIN the minimum number stored per leaf
/// - A the underlying [NodeAllocator] used to store tree nodes and leaves
#[derive(Debug)]
pub struct SSTree<
    T,
    const K: usize,
    F = f64,
    const MAX: usize = 32,
    const MIN: usize = 8,
    A = BiArenaNodeAllocator<T, F, K, MAX>,
> where
    A: NodeAllocator<T, F, K, MAX>,
    T: 'static,
{
    allocator: A,
    root: A::Index,
}

impl<T, const K: usize> SSTree<T, K, f64, 32, 8, BiArenaNodeAllocator<T, f64, K, 32>> {
    ///
    /// Creates a new empty SSTree with the default node allocator
    ///
    pub fn new() -> SSTree<T, K> {
        Self::with_capacity(64)
    }

    ///
    /// Creates a new empty SSTree with the default node allocator, initialized with `capacity`
    ///
    pub fn with_capacity(capacity: usize) -> SSTree<T, K> {
        SSTree::with_allocator(BiArenaNodeAllocator::with_capacity(capacity))
    }
}

impl<T, F, A, const K: usize, const MAX: usize, const MIN: usize> SSTree<T, K, F, MAX, MIN, A>
where
    F: Float,
    A: NodeAllocator<T, F, K, MAX>,
{
    ///
    /// Insert the given [Point] in the tree
    ///
    pub fn insert(&mut self, point: Point<T, F, K>) {
        if let Some(new_child) = self._insert(&self.root.clone(), point) {
            let root_idx = self
                .allocator
                .allocate_node(SSNode::with_children_pair(self.root.clone(), new_child));
            self._update_bounding_envelope(&root_idx);
            self.root = root_idx;
        }
    }

    ///
    /// Retrieve the point data exactly matching the target coordinates
    ///
    pub fn get(&self, target: &[F; K]) -> Option<&T> {
        self._get(&self.root, target)
    }

    ///
    /// Find the nearest neighbor of the target coordinates
    ///
    pub fn neareast_neighbor(&self, target: &[F; K]) -> Option<&Point<T, F, K>> {
        self._nearest_neighbor(&self.root, target, (None, F::infinity()))
            .0
    }

    ///
    /// Find the m-nearest neighbors of the target coordinates
    ///
    pub fn m_neareast_neighbors(&self, target: &[F; K], m: usize) -> Vec<&Point<T, F, K>> {
        let mut mnn_with_distances = MinMElements::new(m);
        self._m_nearest_neighbors(&self.root, target, &mut mnn_with_distances);

        mnn_with_distances
            .into_sorted_vec()
            .iter()
            .map(|nn| nn.0)
            .collect()
    }

    ///
    /// Find all the points in the given region
    ///
    pub fn region_search<R: Region<F, K>>(&self, region: &R) -> Vec<&Point<T, F, K>> {
        let mut result = Vec::new();
        self._region_search(&self.root, region, &mut result);
        result
    }
}

impl<T, F, A, const K: usize, const MAX: usize, const MIN: usize> SSTree<T, K, F, MAX, MIN, A>
where
    F: Float,
    A: NodeAllocator<T, F, K, MAX>,
    T: 'static,
{
    pub fn with_allocator(mut allocator: A) -> Self {
        if MIN > (MAX - 1) / 2 {
            panic!("Min should be <= (Max - 1) / 2");
        }
        let root = allocator.allocate_leaf(SSLeaf::new_empty());
        Self { allocator, root }
    }

    fn _get(&self, node_idx: &A::Index, target: &[F; K]) -> Option<&T> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    if &point.center == target {
                        return Some(&point.data);
                    }
                }
                None
            }
            NodeRef::Node(node) => {
                for child in &node.children {
                    if self._get_bounding_sphere(child).contains_point(target) {
                        let result = self._get(child, target);
                        if result.is_some() {
                            return result;
                        }
                    }
                }
                None
            }
        }
    }

    fn _get_bounding_sphere(&self, node_idx: &A::Index) -> &Sphere<F, K> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Node(node) => &node.bounding_sphere,
            NodeRef::Leaf(leaf) => &leaf.bounding_sphere,
        }
    }

    fn _get_bounding_sphere_mut(&mut self, node_idx: &A::Index) -> &mut Sphere<F, K> {
        match self.allocator.get_node_mut(node_idx) {
            NodeRefMut::Node(node) => &mut node.bounding_sphere,
            NodeRefMut::Leaf(leaf) => &mut leaf.bounding_sphere,
        }
    }

    fn _insert(&mut self, node_idx: &A::Index, point: Point<T, F, K>) -> Option<A::Index> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Leaf(_) => {
                let leaf = self.allocator.get_ssleaf_mut(node_idx);
                leaf.points.push(point);
                let no_split_needed = leaf.points.len() <= MAX - 1;
                self._update_bounding_envelope(node_idx);
                if no_split_needed {
                    return None;
                }
            }
            NodeRef::Node(n) => {
                let closest_child = self._find_closest_child(n, &point.center);
                if let Some(new_child) = self._insert(&closest_child, point) {
                    let mut_node = self.allocator.get_ssnode_mut(node_idx);
                    mut_node.children.push(new_child);

                    let no_split_needed = mut_node.children.len() <= MAX - 1;
                    self._update_bounding_envelope(node_idx);
                    if no_split_needed {
                        return None;
                    }
                } else {
                    self._update_bounding_envelope(node_idx);
                    return None;
                }
            }
        }

        Some(self._split(node_idx))
    }

    fn _get_centroids(&self, node_idx: &A::Index) -> SmallVec<[([F; K], F); MAX]> {
        match self.allocator.get_node(node_idx) {
            NodeRef::Node(node) => node
                .children
                .iter()
                .map(|n| self._get_bounding_sphere(n))
                .map(|s| (s.centroid, s.radius))
                .collect(),
            NodeRef::Leaf(leaf) => leaf
                .points
                .iter()
                .map(|p| (p.center, F::default()))
                .collect(),
        }
    }

    fn _update_bounding_envelope(&mut self, node_idx: &A::Index) {
        let points = self._get_centroids(node_idx);

        let sphere = self._get_bounding_sphere_mut(node_idx);
        for i in 0..K {
            sphere.centroid[i] =
                points.iter().map(|p| p.0[i]).sum::<F>() / F::from_usize(points.len());
        }

        sphere.radius = points
            .iter()
            .map(|entry| F::distance(&sphere.centroid, &entry.0) + entry.1)
            .max_by_key(|f| f.to_ord())
            .unwrap()
    }

    fn _split(&mut self, node: &A::Index) -> A::Index {
        let split_idx = self._find_split_index(node);
        let new_child = match self.allocator.get_node_mut(node) {
            NodeRefMut::Leaf(leaf) => {
                let points: SmallVec<[Point<T, F, K>; MAX]> =
                    leaf.points.drain(split_idx..).collect();

                let new_child = self.allocator.allocate_leaf(SSLeaf {
                    bounding_sphere: Sphere::new_empty(),
                    points,
                });

                new_child
            }
            NodeRefMut::Node(node_ref) => {
                let children: SmallVec<[A::Index; MAX]> =
                    node_ref.children.drain(split_idx..).collect();

                let new_child = self.allocator.allocate_node(SSNode {
                    bounding_sphere: Sphere::new_empty(),
                    children,
                });

                new_child
            }
        };
        self._update_bounding_envelope(&node);
        self._update_bounding_envelope(&new_child);

        new_child
    }

    fn _find_closest_child(&self, node: &SSNode<F, A::Index, K, MAX>, point: &[F; K]) -> A::Index {
        node.children
            .iter()
            .cloned()
            .map(|n| {
                (
                    F::distance(&self._get_bounding_sphere(&n).centroid, point).to_ord(),
                    n,
                )
            })
            .min_by_key(|c| c.0)
            .unwrap()
            .1
    }

    fn _direction_of_max_variance(&self, node_idx: &A::Index) -> usize {
        let centroids = self._get_centroids(node_idx);

        (0..K)
            .max_by_key(|dir| variance_along_directions(&centroids, *dir).to_ord())
            .unwrap()
    }

    fn _find_split_index(&mut self, node_idx: &A::Index) -> usize {
        let index = self._direction_of_max_variance(node_idx);
        self._sort_entries_by_index(node_idx, index);

        let centroids = self._get_centroids(node_idx);

        assert_eq!(centroids.len(), MAX);

        (MIN..=centroids.len() - MIN)
            .min_by_key(|&i| {
                (variance_along_directions(&centroids[0..=i - 1], index)
                    + variance_along_directions(&centroids[i..], index))
                .to_ord()
            })
            .unwrap()
    }

    fn _sort_entries_by_index(&mut self, node_idx: &A::Index, index: usize) {
        match self.allocator.get_node_mut(node_idx) {
            NodeRefMut::Node(node) => {
                let mut children = node.children.clone();
                children.sort_by_key(|p| self._get_bounding_sphere(p).centroid[index].to_ord());
                self.allocator.get_ssnode_mut(node_idx).children = children;
            }
            NodeRefMut::Leaf(leaf) => {
                leaf.points.sort_by_key(|p| p.center[index].to_ord());
            }
        }
    }

    fn _region_search<'a, R: Region<F, K>>(
        &'a self,
        node: &A::Index,
        region: &R,
        result: &mut Vec<&'a Point<T, F, K>>,
    ) {
        match self.allocator.get_node(node) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    if region.contains_point(&point.center) {
                        result.push(point);
                    }
                }
            }
            NodeRef::Node(node) => {
                for child in &node.children {
                    if region.intersects_sphere(self._get_bounding_sphere(child)) {
                        self._region_search(child, region, result);
                    }
                }
            }
        }
    }

    fn _nearest_neighbor<'a>(
        &'a self,
        node: &A::Index,
        target: &[F; K],
        mut nn_with_distance: (Option<&'a Point<T, F, K>>, F),
    ) -> (Option<&'a Point<T, F, K>>, F) {
        match self.allocator.get_node(node) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    let distance = F::distance(&point.center, target);
                    if distance < nn_with_distance.1 {
                        nn_with_distance = (Some(&point), distance);
                    }
                }
            }
            NodeRef::Node(node) => {
                let mut children: SmallVec<[_; MAX]> = node
                    .children
                    .iter()
                    .map(|c| (c, self._get_bounding_sphere(c).distance(target)))
                    .collect();

                children.sort_by_key(|c| c.1.to_ord());
                for child in &children {
                    if child.1.to_ord() < nn_with_distance.1.to_ord() {
                        nn_with_distance =
                            self._nearest_neighbor(child.0, target, nn_with_distance);
                    } else {
                        break; //TODO check
                    }
                }
            }
        }
        nn_with_distance
    }

    fn _m_nearest_neighbors<'a>(
        &'a self,
        node: &A::Index,
        target: &[F; K],
        mnn_with_distances: &mut MinMElements<NN<'a, T, F, K>>,
    ) {
        match self.allocator.get_node(node) {
            NodeRef::Leaf(leaf) => {
                for point in &leaf.points {
                    let distance = F::distance(&point.center, target);
                    mnn_with_distances.add(NN(&point, distance));
                }
            }
            NodeRef::Node(node) => {
                let mut children: SmallVec<[_; MAX]> = node
                    .children
                    .iter()
                    .map(|c| (c, self._get_bounding_sphere(c).distance(target)))
                    .collect();

                children.sort_by_key(|c| c.1.to_ord());
                for child in &children {
                    if !mnn_with_distances.is_filled()
                        || child.1.to_ord() < mnn_with_distances.peek().unwrap().1.to_ord()
                    {
                        self._m_nearest_neighbors(child.0, target, mnn_with_distances);
                    } else {
                        break;
                    }
                }
            }
        }
    }
}
struct NN<'a, T, F, const K: usize>(&'a Point<T, F, K>, F);

impl<'a, T, F, const K: usize> Ord for NN<'a, T, F, K>
where
    F: Float,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.1.to_ord().cmp(&other.1.to_ord())
    }
}

impl<'a, T, F, const K: usize> PartialOrd for NN<'a, T, F, K>
where
    F: Float,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a, T, F, const K: usize> PartialEq for NN<'a, T, F, K>
where
    F: Float,
{
    fn eq(&self, other: &Self) -> bool {
        self.1 == other.1
    }
}

impl<'a, T, F, const K: usize> Eq for NN<'a, T, F, K> where F: Float {}

fn variance_along_directions<F: Float, const K: usize>(points: &[([F; K], F)], i: usize) -> F {
    let n: F = F::from_usize(points.len());
    let mean = points.iter().map(|p| p.0[i]).sum::<F>() / n;
    let var_2 = points
        .iter()
        .map(|p| p.0[i] - mean)
        .map(|f| f * f)
        .sum::<F>()
        / n;
    var_2
}

#[cfg(test)]
mod tests {
    use crate::{
        geometry::{Point, Region, Sphere},
        ss_tree::SSTree,
    };

    fn build_sample_tree() -> SSTree<i32, 3> {
        let mut tree = SSTree::with_capacity(1000);

        let mut i = 0;
        for x in 0..10 {
            for y in 0..10 {
                for z in 0..10 {
                    tree.insert(Point::new(
                        [x as f64 / 100.0, y as f64 / 100.0, z as f64 / 100.0],
                        i,
                    ));
                    i += 1;
                }
            }
        }
        tree
    }

    #[test]
    fn test_get() {
        let tree = build_sample_tree();

        let mut i = 0;
        for x in 0..10 {
            for y in 0..10 {
                for z in 0..10 {
                    let n = *tree
                        .get(&[x as f64 / 100.0, y as f64 / 100.0, z as f64 / 100.0])
                        .unwrap();
                    assert_eq!(n, i);
                    i += 1;
                }
            }
        }
    }

    #[test]
    fn test_nearest_neighbor() {
        let tree = build_sample_tree();
        let nn = tree.neareast_neighbor(&[0.021, 0.057, 0.012]);

        assert!(nn.is_some());
        let nn = nn.unwrap();
        assert_eq!(nn.center, [0.02, 0.06, 0.01]);
        assert_eq!(nn.data, 261);
    }

    #[test]
    fn test_m_nearest_neighbors() {
        let tree = build_sample_tree();
        let nn = tree.m_neareast_neighbors(&[0.021, 0.057, 0.012], 5);

        println!("{:?}", nn);
    }


    #[test]
    fn test_region_search() {
        let tree = build_sample_tree();
        let sphere = Sphere::new([0.05, 0.05, 0.05], 0.025);
        let res = tree.region_search(&sphere);

        let expected_ids = naive_region_search(&sphere);

        let mut found_ids: Vec<i32> = res.iter().map(|p| p.data).collect();
        found_ids.sort();
        assert_eq!(found_ids, expected_ids);
    }

    fn naive_region_search(sphere: &Sphere<f64, 3>) -> Vec<i32> {
        let mut expected_ids = Vec::new();
        let mut i = 0;
        for x in 0..10 {
            for y in 0..10 {
                for z in 0..10 {
                    let p = Point::new([x as f64 / 100.0, y as f64 / 100.0, z as f64 / 100.0], i);
                    i += 1;

                    if sphere.contains_point(&p.center) {
                        expected_ids.push(p.data);
                    }
                }
            }
        }
        expected_ids.sort();
        expected_ids
    }
}
