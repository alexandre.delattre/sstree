use std::collections::BinaryHeap;

pub struct MinMElements<T> {
    m: usize,
    heap: BinaryHeap<T>,
}

impl<T: Ord> MinMElements<T> {
    pub fn new(m: usize) -> Self {
        MinMElements {
            m,
            heap: Default::default(),
        }
    }

    pub fn add(&mut self, item: T) {
        if self.is_filled() {
            if &item < self.heap.peek().unwrap() {
                self.heap.pop();
                self.heap.push(item);
            }
        } else {
            self.heap.push(item);
        }
    }

    pub fn peek(&self) -> Option<&T> {
        self.heap.peek()
    }

    pub fn into_sorted_vec(self) -> Vec<T> {
        self.heap.into_sorted_vec()
    }

    pub fn is_filled(&self) -> bool {
        self.heap.len() == self.m
    } 
}

#[cfg(test)]
mod tests {
    use super::MinMElements;

    #[test]
    fn test_find_min_m_elements() {
        let mut min_m_elems = MinMElements::new(3);
        for i in (1..100).rev() {
            min_m_elems.add(i);
        }
        assert_eq!(min_m_elems.into_sorted_vec(), vec![1, 2, 3]);
    }
}
