use crate::float_trait::Float;

pub trait Region<F, const K: usize> {
    fn contains_point(&self, point: &[F; K]) -> bool;
    fn intersects_sphere(&self, other: &Sphere<F, K>) -> bool;
}

#[derive(Debug)]
pub struct Sphere<F, const K: usize> {
    pub centroid: [F; K],
    pub radius: F,
}

impl<F, const K: usize> Sphere<F, K>
where
    F: Float,
{
    pub fn new(centroid: [F; K], radius: F) -> Self {
        Self { centroid, radius }
    }

    pub fn new_empty() -> Sphere<F, K> {
        let centroid = [F::default(); K];
        let radius = F::default();
        Sphere { centroid, radius }
    }

    pub fn distance(&self, point: &[F; K]) -> F {
        F::distance(&self.centroid, point) - self.radius
    }
}

impl<F, const K: usize> Region<F, K> for Sphere<F, K>
where
    F: Float,
{
    fn contains_point(&self, point: &[F; K]) -> bool {
        self.distance(point) <= F::default()
    }

    fn intersects_sphere(&self, other: &Sphere<F, K>) -> bool {
        F::distance(&self.centroid, &other.centroid) <= self.radius + other.radius
    }
}

pub struct Box<F, const K: usize> {
    pub min: [F; K],
    pub max: [F; K],
}

impl<F, const K: usize> Box<F, K> {
    pub fn new(min: [F; K], max: [F; K]) -> Self {
        Self { min, max }
    }
}

impl<F, const K: usize> Region<F, K> for Box<F, K>
where
    F: Float,
{
    fn contains_point(&self, point: &[F; K]) -> bool {
        point
            .iter()
            .enumerate()
            .all(|(i, f)| f >= &self.min[i] && f <= &self.max[i])
    }

    fn intersects_sphere(&self, other: &Sphere<F, K>) -> bool {
        let mut nearest = [F::default(); K];
        for i in 0..K {
            nearest[i] = other.centroid[i].clamp(self.min[i], self.max[i]);
        }
        F::distance(&nearest, &other.centroid) <= other.radius
    }
}

#[derive(Debug, Clone)]
pub struct Point<T, F, const K: usize> {
    pub center: [F; K],
    pub data: T,
}

impl<T, F, const K: usize> Point<T, F, K> {
    pub fn new(center: [F; K], data: T) -> Self {
        Self { center, data }
    }
}
