build: build-rust build-js

build-rust:
    cd sstree-wasm;  RUSTFLAGS="-C target-feature=+simd128" wasm-pack build -t nodejs --release 

build-js:
    cd sstree-example; npx nx build --configuration=production

run *args:
    cd sstree-example; node {{args}} dist/apps/example/main 

serve:
    cd sstree-example; npx nx serve

test:
    cargo test --all
    cd sstree-example; npx nx test

doc:
    cd sstree; cargo doc --open

clean:
    cd sstree; cargo clean
    cd sstree-wasm; cargo clean
    cd sstree-example; rm -R dist